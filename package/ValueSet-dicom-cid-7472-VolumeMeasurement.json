{
  "resourceType": "ValueSet",
  "id": "dicom-cid-7472-VolumeMeasurement",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_7472.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.526"
    }
  ],
  "version": "20190121",
  "name": "VolumeMeasurement",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 7472 VolumeMeasurement",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://arxiv.org/abs/1612.07003",
        "concept": [
          {
            "code": "YEKZ",
            "display": "Volume from Voxel Summation"
          },
          {
            "code": "RNU0",
            "display": "Volume of Mesh"
          }
        ]
      },
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "121217",
            "display": "Volume estimated from three or more non-coplanar 2D regions"
          },
          {
            "code": "121218",
            "display": "Volume estimated from two non-coplanar 2D regions"
          },
          {
            "code": "121216",
            "display": "Volume estimated from single 2D region"
          },
          {
            "code": "121222",
            "display": "Volume of sphere"
          },
          {
            "code": "121220",
            "display": "Volume of circumscribed sphere"
          },
          {
            "code": "121221",
            "display": "Volume of ellipsoid"
          },
          {
            "code": "121219",
            "display": "Volume of bounding three dimensional region"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "118565006",
            "display": "Volume"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "VolumeMeasurement",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>VolumeMeasurement</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_7472.html</tt><p>Transitive closure of CID 7472 VolumeMeasurement</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}