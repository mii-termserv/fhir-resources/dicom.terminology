{
  "resourceType": "ValueSet",
  "id": "dicom-cid-3405-ProcedureActionValue",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_3405.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.70"
    }
  ],
  "version": "20190817",
  "name": "ProcedureActionValue",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 3405 ProcedureActionValue",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "122058",
            "display": "Arterial conduit angiography"
          },
          {
            "code": "122056",
            "display": "Vascular Intervention"
          },
          {
            "code": "122057",
            "display": "Myocardial biopsy"
          },
          {
            "code": "122054",
            "display": "Aortic Intervention"
          },
          {
            "code": "122055",
            "display": "Septal Defect Intervention"
          },
          {
            "code": "122053",
            "display": "Valvular Intervention"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "128955008",
            "display": "Cardiac catheterization baseline phase"
          },
          {
            "code": "433236007",
            "display": "Transthoracic echocardiography"
          },
          {
            "code": "103716009",
            "display": "Stent placement"
          },
          {
            "code": "252426003",
            "display": "Cardiac ventriculography"
          },
          {
            "code": "67338003",
            "display": "Transseptal catheterization"
          },
          {
            "code": "133882006",
            "display": "Drug Infusion Challenge"
          },
          {
            "code": "65659003",
            "display": "Atherectomy by rotary cutter"
          },
          {
            "code": "240946003",
            "display": "Percutaneous removal of intravascular foreign body"
          },
          {
            "code": "128957000",
            "display": "Cardiac catheterization intervention phase"
          },
          {
            "code": "128959002",
            "display": "Cardiac catheterization therapy phase"
          },
          {
            "code": "265484009",
            "display": "Left Ventriculography"
          },
          {
            "code": "128953001",
            "display": "Catheterization of both left and right heart without graft"
          },
          {
            "code": "241466007",
            "display": "Intravascular ultrasound"
          },
          {
            "code": "129083002",
            "display": "Cardiac catheterization post contrast phase"
          },
          {
            "code": "54640009",
            "display": "Aortography"
          },
          {
            "code": "67629009",
            "display": "Catheterization of left heart"
          },
          {
            "code": "128960007",
            "display": "Cardiac catheterization post-intervention phase"
          },
          {
            "code": "105373008",
            "display": "Percutaneous insertion of intravascular filter"
          },
          {
            "code": "128956009",
            "display": "Cardiac catheterization image acquisition phase"
          },
          {
            "code": "128952006",
            "display": "Catheterization of both left and right heart with graft"
          },
          {
            "code": "76611008",
            "display": "Atherectomy by laser"
          },
          {
            "code": "265483003",
            "display": "Right Ventriculography"
          },
          {
            "code": "16736007",
            "display": "Transcatheter therapy for embolization"
          },
          {
            "code": "128975004",
            "display": "Resting State"
          },
          {
            "code": "128958005",
            "display": "Cardiac catheterization pre-intervention phase"
          },
          {
            "code": "6832004",
            "display": "Atherectomy"
          },
          {
            "code": "57238002",
            "display": "Selective embolization of artery"
          },
          {
            "code": "252421008",
            "display": "Intracardiac echocardiography"
          },
          {
            "code": "33367005",
            "display": "Coronary Arteriography"
          },
          {
            "code": "373105002",
            "display": "Cardiac catheterization test/challenge phase"
          },
          {
            "code": "68457009",
            "display": "Percutaneous transluminal balloon angioplasty"
          },
          {
            "code": "433232009",
            "display": "Epicardial echocardiography"
          },
          {
            "code": "252427007",
            "display": "Bypass graft angiography"
          },
          {
            "code": "128961006",
            "display": "Cardiac catheterization bailout phase"
          },
          {
            "code": "77343006",
            "display": "Angiography"
          },
          {
            "code": "105376000",
            "display": "Transesophageal echocardiography"
          },
          {
            "code": "105372003",
            "display": "Transcatheter deployment of detachable balloon"
          },
          {
            "code": "40403005",
            "display": "Catheterization of right heart"
          },
          {
            "code": "128967005",
            "display": "Exercise challenge"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "ProcedureActionValue",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>ProcedureActionValue</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_3405.html</tt><p>Transitive closure of CID 3405 ProcedureActionValue</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}