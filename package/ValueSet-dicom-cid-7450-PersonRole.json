{
  "resourceType": "ValueSet",
  "id": "dicom-cid-7450-PersonRole",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_7450.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.514"
    }
  ],
  "version": "20040112",
  "name": "PersonRole",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 7450 PersonRole",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "121025",
            "display": "Patient"
          },
          {
            "code": "128673",
            "display": "Administrator of Radiology Department"
          },
          {
            "code": "128675",
            "display": "Head of Cardiology"
          },
          {
            "code": "128674",
            "display": "Lead Radiologic Technologist"
          },
          {
            "code": "128677",
            "display": "Representative of Ethics Committee"
          },
          {
            "code": "128676",
            "display": "Representative of Protocol Committee"
          },
          {
            "code": "121088",
            "display": "Fellow"
          },
          {
            "code": "128671",
            "display": "Chair of Protocol Committee"
          },
          {
            "code": "128670",
            "display": "Head of Radiology"
          },
          {
            "code": "121092",
            "display": "Sonologist"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "62296006",
            "display": "Natural grand-father"
          },
          {
            "code": "11993008",
            "display": "Male first cousin"
          },
          {
            "code": "9947008",
            "display": "Natural father"
          },
          {
            "code": "223366009",
            "display": "Healthcare professional"
          },
          {
            "code": "2272004",
            "display": "Half-sister"
          },
          {
            "code": "65656005",
            "display": "Natural mother"
          },
          {
            "code": "405277009",
            "display": "Resident"
          },
          {
            "code": "73678001",
            "display": "Natural sister"
          },
          {
            "code": "113163005",
            "display": "Friend"
          },
          {
            "code": "309343006",
            "display": "Physician"
          },
          {
            "code": "405279007",
            "display": "Attending"
          },
          {
            "code": "60614009",
            "display": "Natural brother"
          },
          {
            "code": "415506007",
            "display": "Scrub nurse"
          },
          {
            "code": "158971006",
            "display": "Registrar"
          },
          {
            "code": "159016003",
            "display": "Radiologic Technologist"
          },
          {
            "code": "25211005",
            "display": "Aunt"
          },
          {
            "code": "304292004",
            "display": "Surgeon"
          },
          {
            "code": "17945006",
            "display": "Natural grand-mother"
          },
          {
            "code": "38048003",
            "display": "Uncle"
          },
          {
            "code": "309390008",
            "display": "Consultant"
          },
          {
            "code": "106292003",
            "display": "Nurse"
          },
          {
            "code": "83420006",
            "display": "Natural daughter"
          },
          {
            "code": "113160008",
            "display": "Natural son"
          },
          {
            "code": "270002",
            "display": "Female first cousin"
          },
          {
            "code": "158965000",
            "display": "Medical Practitioner"
          },
          {
            "code": "45929001",
            "display": "Half-brother"
          },
          {
            "code": "3430008",
            "display": "Radiation Therapist"
          }
        ]
      },
      {
        "system": "http://www.nlm.nih.gov/research/umls",
        "concept": [
          {
            "code": "C1144859",
            "display": "Intern"
          },
          {
            "code": "C1708969",
            "display": "Medical Physicist"
          },
          {
            "code": "C1441532",
            "display": "Consulting Physician"
          },
          {
            "code": "C2985483",
            "display": "Radiation Physicist"
          },
          {
            "code": "C1954848",
            "display": "Sonographer"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "PersonRole",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>PersonRole</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_7450.html</tt><p>Transitive closure of CID 7450 PersonRole</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}