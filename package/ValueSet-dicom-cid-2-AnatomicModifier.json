{
  "resourceType": "ValueSet",
  "id": "dicom-cid-2-AnatomicModifier",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_2.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.1"
    }
  ],
  "version": "20190118",
  "name": "AnatomicModifier",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 2 AnatomicModifier",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "130290",
            "display": "Median"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "14414005",
            "display": "Peripheral"
          },
          {
            "code": "57195005",
            "display": "Basal"
          },
          {
            "code": "51440002",
            "display": "Bilateral"
          },
          {
            "code": "24028007",
            "display": "Right"
          },
          {
            "code": "410679008",
            "display": "Surface"
          },
          {
            "code": "3583002",
            "display": "Caudal"
          },
          {
            "code": "255551008",
            "display": "Posterior"
          },
          {
            "code": "33096000",
            "display": "Vertical"
          },
          {
            "code": "30730003",
            "display": "Sagittal"
          },
          {
            "code": "7771000",
            "display": "Left"
          },
          {
            "code": "261074009",
            "display": "External"
          },
          {
            "code": "66459002",
            "display": "Unilateral"
          },
          {
            "code": "264217000",
            "display": "Superior"
          },
          {
            "code": "131183008",
            "display": "Intra-articular"
          },
          {
            "code": "57183005",
            "display": "Edge"
          },
          {
            "code": "46053002",
            "display": "Distal"
          },
          {
            "code": "40415009",
            "display": "Proximal"
          },
          {
            "code": "24422004",
            "display": "Axial"
          },
          {
            "code": "112233002",
            "display": "Marginal"
          },
          {
            "code": "32400000",
            "display": "Preaxial"
          },
          {
            "code": "795002",
            "display": "Deep"
          },
          {
            "code": "61397002",
            "display": "Subcapsular"
          },
          {
            "code": "49370004",
            "display": "Lateral"
          },
          {
            "code": "261089000",
            "display": "Inferior"
          },
          {
            "code": "11070000",
            "display": "Capsular"
          },
          {
            "code": "255549009",
            "display": "Anterior"
          },
          {
            "code": "11896004",
            "display": "Intermediate"
          },
          {
            "code": "62824007",
            "display": "Transverse"
          },
          {
            "code": "66787007",
            "display": "Cephalic"
          },
          {
            "code": "37197008",
            "display": "Anterolateral"
          },
          {
            "code": "26216008",
            "display": "Central"
          },
          {
            "code": "32381004",
            "display": "Hilar"
          },
          {
            "code": "43674008",
            "display": "Apical"
          },
          {
            "code": "87687004",
            "display": "Extra-articular"
          },
          {
            "code": "60583000",
            "display": "Postaxial"
          },
          {
            "code": "24020000",
            "display": "Horizontal"
          },
          {
            "code": "90069004",
            "display": "Posterolateral"
          },
          {
            "code": "49530007",
            "display": "Afferent"
          },
          {
            "code": "26283006",
            "display": "Superficial"
          },
          {
            "code": "68493006",
            "display": "Gutter"
          },
          {
            "code": "81654009",
            "display": "Coronal"
          },
          {
            "code": "33843005",
            "display": "Efferent"
          },
          {
            "code": "260521003",
            "display": "Internal"
          },
          {
            "code": "255561001",
            "display": "Medial"
          },
          {
            "code": "38717003",
            "display": "Longitudinal"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "AnatomicModifier",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>AnatomicModifier</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_2.html</tt><p>Transitive closure of CID 2 AnatomicModifier</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}