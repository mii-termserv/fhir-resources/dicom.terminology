{
  "resourceType": "ValueSet",
  "id": "dicom-cid-6050-BreastProcedureReported",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_6050.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.379"
    }
  ],
  "version": "20190912",
  "name": "BreastProcedureReported",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 6050 BreastProcedureReported",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "111123",
            "display": "Marker placement"
          },
          {
            "code": "111408",
            "display": "Film Screen Mammography"
          },
          {
            "code": "111410",
            "display": "Surgical consult"
          },
          {
            "code": "111411",
            "display": "Mammography CAD"
          }
        ]
      },
      {
        "system": "http://loinc.org",
        "concept": [
          {
            "code": "37774-7",
            "display": "breast - right mammogram"
          },
          {
            "code": "46342-2",
            "display": "breast ffd mammogram"
          },
          {
            "code": "43528-9",
            "display": "breast - unilateral mr wo and w contrast iv"
          },
          {
            "code": "36626-0",
            "display": "breast - bilateral mammogram"
          },
          {
            "code": "44139-4",
            "display": "whole body pt w rnc iv"
          },
          {
            "code": "46339-8",
            "display": "breast - unilateral mammogram"
          },
          {
            "code": "36149-3",
            "display": "breast mr w contrast iv"
          },
          {
            "code": "46323-2",
            "display": "breast - unilateral mr w contrast iv"
          },
          {
            "code": "36150-1",
            "display": "breast - bilateral mr w contrast iv"
          },
          {
            "code": "36279-8",
            "display": "breast - right mr wo and w contrast iv"
          },
          {
            "code": "46333-1",
            "display": "breast - unilateral mr wo contrast"
          },
          {
            "code": "35954-7",
            "display": "breast - left mr"
          },
          {
            "code": "36278-0",
            "display": "breast - left mr wo and w contrast iv"
          },
          {
            "code": "36276-4",
            "display": "breast mr wo and w contrast iv"
          },
          {
            "code": "35955-4",
            "display": "breast - right mr"
          },
          {
            "code": "36277-2",
            "display": "breast - bilateral mr wo and w contrast iv"
          },
          {
            "code": "46305-9",
            "display": "whole body ct"
          },
          {
            "code": "30795-9",
            "display": "breast - bilateral mr"
          },
          {
            "code": "36627-8",
            "display": "breast - left mammogram"
          },
          {
            "code": "36151-9",
            "display": "breast - left mr w contrast iv"
          },
          {
            "code": "36152-7",
            "display": "breast - right mr w contrast iv"
          },
          {
            "code": "46299-4",
            "display": "breast - unilateral mr"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "46662001",
            "display": "Examination of breast"
          },
          {
            "code": "387736007",
            "display": "Fine needle aspiration of breast"
          },
          {
            "code": "241615005",
            "display": "MRI of breast"
          },
          {
            "code": "80865008",
            "display": "Specimen radiography of breast"
          },
          {
            "code": "18102001",
            "display": "Mammary ductogram"
          },
          {
            "code": "47079000",
            "display": "Ultrasonography of breast"
          },
          {
            "code": "237380007",
            "display": "Pre-biopsy localization of breast lesion"
          },
          {
            "code": "274331003",
            "display": "Breast - surgical biopsy"
          },
          {
            "code": "45211000",
            "display": "Insertion of catheter"
          },
          {
            "code": "44578009",
            "display": "Core needle biopsy of breast"
          },
          {
            "code": "396487001",
            "display": "Sentinel lymph node biopsy"
          },
          {
            "code": "241539009",
            "display": "CT of breast"
          },
          {
            "code": "169167001",
            "display": "Radioisotope scan of lymphatic system"
          },
          {
            "code": "287572003",
            "display": "Diagnostic aspiration of breast cyst"
          },
          {
            "code": "66377006",
            "display": "Radionuclide localization of tumor, limited area"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "BreastProcedureReported",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>BreastProcedureReported</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_6050.html</tt><p>Transitive closure of CID 6050 BreastProcedureReported</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}