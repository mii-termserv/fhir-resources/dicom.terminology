{
  "resourceType": "ValueSet",
  "id": "dicom-cid-12273-CardiacUltrasoundAorticSinotubularJunctionMeasur",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_12273.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.854"
    }
  ],
  "version": "20100317",
  "name": "CardiacUltrasoundAorticSinotubularJunctionMeasurement",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 12273 CardiacUltrasoundAorticSinotubularJunctionMeasurement",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://loinc.org",
        "concept": [
          {
            "code": "59127-1",
            "display": "D-E Slope"
          },
          {
            "code": "59103-2",
            "display": "A-C Interval"
          },
          {
            "code": "59104-0",
            "display": "Peak E wave/Peak A wave by US"
          },
          {
            "code": "59080-2",
            "display": "E-Wave Peak Velocity"
          },
          {
            "code": "20256-4",
            "display": "Mean Gradient"
          },
          {
            "code": "20354-7",
            "display": "Velocity Time Integral"
          },
          {
            "code": "59081-0",
            "display": "A-Wave Peak Velocity"
          },
          {
            "code": "11726-7",
            "display": "Peak Systolic Velocity"
          },
          {
            "code": "59106-5",
            "display": "Stenosis Peak Gradient"
          },
          {
            "code": "59107-3",
            "display": "Stenosis Peak Velocity"
          },
          {
            "code": "33878-0",
            "display": "Volume Flow"
          },
          {
            "code": "59102-4",
            "display": "Flow Radius"
          },
          {
            "code": "8867-4",
            "display": "Heart rate"
          },
          {
            "code": "11692-1",
            "display": "Time Averaged Peak Velocity"
          },
          {
            "code": "59089-3",
            "display": "ROI Thickness by US"
          },
          {
            "code": "11653-3",
            "display": "End Diastolic Velocity"
          },
          {
            "code": "59116-4",
            "display": "Aortic Sinotubular Junction to Aortic Root Ratio"
          },
          {
            "code": "59090-1",
            "display": "ROI Internal Dimension by US"
          },
          {
            "code": "59115-6",
            "display": "Velocity of Flow Propagation"
          },
          {
            "code": "20247-3",
            "display": "Peak Gradient"
          },
          {
            "code": "59130-5",
            "display": "Alias velocity"
          },
          {
            "code": "20280-4",
            "display": "Pressure Half-Time"
          },
          {
            "code": "59111-5",
            "display": "E Velocity to Annulus E Velocity Ratio"
          },
          {
            "code": "34141-2",
            "display": "Peak Instantaneous Flow Rate"
          },
          {
            "code": "20167-3",
            "display": "Acceleration Slope"
          },
          {
            "code": "20216-8",
            "display": "Deceleration Slope"
          },
          {
            "code": "20217-6",
            "display": "Deceleration Time"
          },
          {
            "code": "20168-1",
            "display": "Acceleration Time"
          },
          {
            "code": "20355-4",
            "display": "Peak Blood Velocity"
          },
          {
            "code": "12144-2",
            "display": "Systolic to Diastolic Velocity Ratio"
          },
          {
            "code": "59128-9",
            "display": "E-F Slope"
          },
          {
            "code": "59079-4",
            "display": "Peak Reversal Velocity during Atrial Contraction"
          },
          {
            "code": "20352-1",
            "display": "Time Averaged Mean Velocity"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "131187009",
            "display": "Major Axis"
          },
          {
            "code": "399301000",
            "display": "Regurgitant Fraction"
          },
          {
            "code": "131190003",
            "display": "Radius"
          },
          {
            "code": "131188004",
            "display": "Minor Axis"
          },
          {
            "code": "81827009",
            "display": "Diameter"
          },
          {
            "code": "399367004",
            "display": "Cardiovascular Orifice Area"
          },
          {
            "code": "399027007",
            "display": "Cardiovascular Orifice Diameter"
          },
          {
            "code": "410668003",
            "display": "Length"
          },
          {
            "code": "74551000",
            "display": "Circumference"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "CardiacUltrasoundAorticSinotubularJunctionMeasurement",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>CardiacUltrasoundAorticSinotubularJunctionMeasurement</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_12273.html</tt><p>Transitive closure of CID 12273 CardiacUltrasoundAorticSinotubularJunctionMeasurement</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}