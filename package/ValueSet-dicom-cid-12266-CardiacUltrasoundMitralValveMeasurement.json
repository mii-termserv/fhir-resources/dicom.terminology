{
  "resourceType": "ValueSet",
  "id": "dicom-cid-12266-CardiacUltrasoundMitralValveMeasurement",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_12266.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.847"
    }
  ],
  "version": "20100317",
  "name": "CardiacUltrasoundMitralValveMeasurement",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 12266 CardiacUltrasoundMitralValveMeasurement",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "109072",
            "display": "Tau"
          },
          {
            "code": "109071",
            "display": "Indicator mean transit time"
          },
          {
            "code": "122182",
            "display": "R-R interval"
          },
          {
            "code": "122207",
            "display": "Blood velocity, peak"
          },
          {
            "code": "122205",
            "display": "Blood velocity, mean"
          },
          {
            "code": "122206",
            "display": "Blood velocity, minimum"
          },
          {
            "code": "122203",
            "display": "Systolic blood velocity, mean"
          },
          {
            "code": "122204",
            "display": "Systolic blood velocity, peak"
          },
          {
            "code": "122201",
            "display": "Diastolic blood velocity, mean"
          },
          {
            "code": "122202",
            "display": "Diastolic blood velocity, peak"
          }
        ]
      },
      {
        "system": "http://loinc.org",
        "concept": [
          {
            "code": "59127-1",
            "display": "D-E Slope"
          },
          {
            "code": "59104-0",
            "display": "Peak E wave/Peak A wave by US"
          },
          {
            "code": "59080-2",
            "display": "E-Wave Peak Velocity"
          },
          {
            "code": "20256-4",
            "display": "Mean Gradient"
          },
          {
            "code": "20354-7",
            "display": "Velocity Time Integral"
          },
          {
            "code": "59106-5",
            "display": "Stenosis Peak Gradient"
          },
          {
            "code": "59123-0",
            "display": "Jet Area"
          },
          {
            "code": "59108-1",
            "display": "Envelope Duration"
          },
          {
            "code": "33878-0",
            "display": "Volume Flow"
          },
          {
            "code": "59121-4",
            "display": "Time to Peak by US"
          },
          {
            "code": "59102-4",
            "display": "Flow Radius"
          },
          {
            "code": "59086-9",
            "display": "Heart Rate-Corrected Ejection Time"
          },
          {
            "code": "8867-4",
            "display": "Heart rate"
          },
          {
            "code": "59088-5",
            "display": "Pre-Ejection Period/Ejection Time Ratio"
          },
          {
            "code": "20226-7",
            "display": "Flow Area"
          },
          {
            "code": "11653-3",
            "display": "End Diastolic Velocity"
          },
          {
            "code": "59084-4",
            "display": "Isovolumic Contraction Time"
          },
          {
            "code": "59082-8",
            "display": "Closure to Opening Time"
          },
          {
            "code": "59110-7",
            "display": "Leaflet Thickness"
          },
          {
            "code": "59118-0",
            "display": "HR-Corrected Mean Velocity of Circumferential Fiber Shortening"
          },
          {
            "code": "59090-1",
            "display": "ROI Internal Dimension by US"
          },
          {
            "code": "20247-3",
            "display": "Peak Gradient"
          },
          {
            "code": "20222-6",
            "display": "Ejection Time"
          },
          {
            "code": "18035-6",
            "display": "Mitral Regurgitation dP/dt derived from Mitral Reg. velocity"
          },
          {
            "code": "34141-2",
            "display": "Peak Instantaneous Flow Rate"
          },
          {
            "code": "20217-6",
            "display": "Deceleration Time"
          },
          {
            "code": "59109-9",
            "display": "Leaflet Separation"
          },
          {
            "code": "20168-1",
            "display": "Acceleration Time"
          },
          {
            "code": "59098-4",
            "display": "Mitral Valve E-septal Separation"
          },
          {
            "code": "20355-4",
            "display": "Peak Blood Velocity"
          },
          {
            "code": "59128-9",
            "display": "E-F Slope"
          },
          {
            "code": "59079-4",
            "display": "Peak Reversal Velocity during Atrial Contraction"
          },
          {
            "code": "59092-7",
            "display": "% Thickening"
          },
          {
            "code": "59103-2",
            "display": "A-C Interval"
          },
          {
            "code": "59085-1",
            "display": "Pre-Ejection Period"
          },
          {
            "code": "59081-0",
            "display": "A-Wave Peak Velocity"
          },
          {
            "code": "59105-7",
            "display": "A-Wave Duration"
          },
          {
            "code": "11726-7",
            "display": "Peak Systolic Velocity"
          },
          {
            "code": "59107-3",
            "display": "Stenosis Peak Velocity"
          },
          {
            "code": "59122-2",
            "display": "C-E Distance"
          },
          {
            "code": "59120-6",
            "display": "dP/dt by US"
          },
          {
            "code": "11692-1",
            "display": "Time Averaged Peak Velocity"
          },
          {
            "code": "59089-3",
            "display": "ROI Thickness by US"
          },
          {
            "code": "59119-8",
            "display": "Filling Time"
          },
          {
            "code": "59087-7",
            "display": "Heart Rate-Corrected Pre-Ejection Period"
          },
          {
            "code": "59083-6",
            "display": "Isovolumic Relaxation Time"
          },
          {
            "code": "59117-2",
            "display": "Mean Velocity of Circumferential Fiber Shortening (Mean VcFv)"
          },
          {
            "code": "59115-6",
            "display": "Velocity of Flow Propagation"
          },
          {
            "code": "18036-4",
            "display": "Mitral Valve EPSS, E wave"
          },
          {
            "code": "59132-1",
            "display": "Fractional Shortening"
          },
          {
            "code": "59130-5",
            "display": "Alias velocity"
          },
          {
            "code": "20280-4",
            "display": "Pressure Half-Time"
          },
          {
            "code": "59111-5",
            "display": "E Velocity to Annulus E Velocity Ratio"
          },
          {
            "code": "20167-3",
            "display": "Acceleration Slope"
          },
          {
            "code": "20216-8",
            "display": "Deceleration Slope"
          },
          {
            "code": "12144-2",
            "display": "Systolic to Diastolic Velocity Ratio"
          },
          {
            "code": "59091-9",
            "display": "D-E Excursion"
          },
          {
            "code": "20352-1",
            "display": "Time Averaged Mean Velocity"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "131187009",
            "display": "Major Axis"
          },
          {
            "code": "42798000",
            "display": "Area"
          },
          {
            "code": "82799009",
            "display": "Cardiac Output"
          },
          {
            "code": "70822001",
            "display": "Cardiac ejection fraction"
          },
          {
            "code": "399027007",
            "display": "Cardiovascular Orifice Diameter"
          },
          {
            "code": "410668003",
            "display": "Length"
          },
          {
            "code": "277381004",
            "display": "Stroke Index"
          },
          {
            "code": "74551000",
            "display": "Circumference"
          },
          {
            "code": "75367002",
            "display": "Blood Pressure"
          },
          {
            "code": "54993008",
            "display": "Cardiac Index"
          },
          {
            "code": "399301000",
            "display": "Regurgitant Fraction"
          },
          {
            "code": "131190003",
            "display": "Radius"
          },
          {
            "code": "131188004",
            "display": "Minor Axis"
          },
          {
            "code": "81827009",
            "display": "Diameter"
          },
          {
            "code": "90096001",
            "display": "Stroke Volume"
          },
          {
            "code": "399367004",
            "display": "Cardiovascular Orifice Area"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "CardiacUltrasoundMitralValveMeasurement",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>CardiacUltrasoundMitralValveMeasurement</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_12266.html</tt><p>Transitive closure of CID 12266 CardiacUltrasoundMitralValveMeasurement</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}