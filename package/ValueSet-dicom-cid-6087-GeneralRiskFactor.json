{
  "resourceType": "ValueSet",
  "id": "dicom-cid-6087-GeneralRiskFactor",
  "url": "http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_6087.html",
  "identifier": [
    {
      "system": "urn:ietf:rfc:3986",
      "value": "urn:oid:1.2.840.10008.6.1.409"
    }
  ],
  "version": "20040112",
  "name": "GeneralRiskFactor",
  "status": "active",
  "experimental": false,
  "date": "2024-06-17",
  "publisher": "NEMA MITA DICOM",
  "description": "Transitive closure of CID 6087 GeneralRiskFactor",
  "copyright": "© 2024 NEMA",
  "compose": {
    "include": [
      {
        "system": "http://dicom.nema.org/resources/ontology/DCM",
        "concept": [
          {
            "code": "111560",
            "display": "Intermediate family history of breast cancer"
          },
          {
            "code": "111561",
            "display": "Very strong family history of breast cancer"
          },
          {
            "code": "111562",
            "display": "Family history of prostate cancer"
          },
          {
            "code": "111563",
            "display": "Family history unknown"
          },
          {
            "code": "111565",
            "display": "Uterine malformations"
          },
          {
            "code": "111566",
            "display": "Spontaneous Abortion"
          },
          {
            "code": "111567",
            "display": "Gynecologic condition"
          },
          {
            "code": "111568",
            "display": "Gynecologic surgery"
          },
          {
            "code": "111569",
            "display": "Previous LBW or IUGR birth"
          },
          {
            "code": "111570",
            "display": "Previous fetal malformation/syndrome"
          },
          {
            "code": "111571",
            "display": "Previous RH negative or blood dyscrasia at birth"
          },
          {
            "code": "111572",
            "display": "History of multiple fetuses"
          },
          {
            "code": "111550",
            "display": "Personal breast cancer history"
          },
          {
            "code": "111573",
            "display": "Current pregnancy, known or suspected malformations/syndromes"
          },
          {
            "code": "111551",
            "display": "History of endometrial cancer"
          },
          {
            "code": "111574",
            "display": "Family history, fetal malformation/syndrome"
          },
          {
            "code": "111552",
            "display": "History of ovarian cancer"
          },
          {
            "code": "111553",
            "display": "History of high risk lesion on previous biopsy"
          },
          {
            "code": "111554",
            "display": "Post menopausal patient"
          },
          {
            "code": "111555",
            "display": "Late child bearing (after 30)"
          },
          {
            "code": "111556",
            "display": "BRCA1 breast cancer gene"
          },
          {
            "code": "111557",
            "display": "BRCA2 breast cancer gene"
          },
          {
            "code": "111558",
            "display": "BRCA3 breast cancer gene"
          },
          {
            "code": "111559",
            "display": "Weak family history of breast cancer"
          }
        ]
      },
      {
        "system": "http://snomed.info/sct",
        "concept": [
          {
            "code": "161445009",
            "display": "History of - diabetes mellitus"
          },
          {
            "code": "161806007",
            "display": "History of - eclampsia"
          },
          {
            "code": "161798008",
            "display": "History of infertility"
          },
          {
            "code": "429740004",
            "display": "Family history of breast cancer"
          },
          {
            "code": "266995000",
            "display": "History of - cardiovascular disease"
          },
          {
            "code": "161453001",
            "display": "History of - obesity"
          },
          {
            "code": "161501007",
            "display": "History of - hypertension"
          },
          {
            "code": "371422002",
            "display": "History of substance abuse"
          },
          {
            "code": "161763005",
            "display": "History of - ectopic pregnancy"
          },
          {
            "code": "102877006",
            "display": "Nulliparous"
          },
          {
            "code": "161765003",
            "display": "History of - premature delivery"
          },
          {
            "code": "161656000",
            "display": "History of - regular medication"
          },
          {
            "code": "161807003",
            "display": "History of - severe pre-eclampsia"
          },
          {
            "code": "16356006",
            "display": "Multiple pregnancy"
          },
          {
            "code": "313376005",
            "display": "No family history of breast carcinoma"
          }
        ]
      }
    ]
  },
  "meta": {
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "dicom",
        "display": "Digital Imaging and Communications in Medicine"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "international",
        "display": "International standard terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "dicom",
        "display": "DICOM Terminology"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "standard",
        "display": "International Standards"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses#dicom",
        "display": "DICOM license"
      }
    ]
  },
  "title": "GeneralRiskFactor",
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>GeneralRiskFactor</h2><tt>http://dicom.nema.org/medical/dicom/current/output/chtml/part16/sect_CID_6087.html</tt><p>Transitive closure of CID 6087 GeneralRiskFactor</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Projects</b></td><td><ul><li>Digital Imaging and Communications in Medicine</li><li>International standard terminology</li></ul></td></tr><tr><td><b>Datasets</b></td><td><ul><li>DICOM Terminology</li><li>International Standards</li></ul></td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses#dicom\">DICOM license</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/dicom.terminology\"><code>@mii-termserv/dicom.terminology</code></a></td></tr></tbody></table></div></div>"
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/dicom.terminology"
    }
  ]
}