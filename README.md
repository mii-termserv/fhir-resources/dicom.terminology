# DICOM ValueSets and DCM Ontology

This package redistributes the ValueSets from ftp://medical.nema.org/medical/dicom/resources/valuesets as of 2024-06-19 and the DICOM DCM Ontology from the FHIR R4B standard.